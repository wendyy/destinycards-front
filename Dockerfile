FROM nginx:alpine

ADD img /usr/share/nginx/html/img
ADD js /usr/share/nginx/html/js
ADD css /usr/share/nginx/html/css
COPY test.json /usr/share/nginx/html
COPY index.html /usr/share/nginx/html